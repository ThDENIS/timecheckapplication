package centralApplication.view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;

import companyModel.Company;
import companyModel.Department;
import companyModel.SerialSaving;

/**
 * The Class RemoveDepartmentWindow, the GUI to add a department in the company.
 */
public class RemoveDepartmentWindow extends JFrame implements Runnable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/**
	 * The method run that opens the GUI.
	 */
	@Override
	public void run() {
		this.setTitle("Remove Department");
		this.setSize(500, 300);	
		Box TextBox = Box.createHorizontalBox();
		
		JComboBox<Department> combo = new JComboBox<Department>();
		combo.setMaximumSize(new Dimension(400, 25));

		ArrayList<Department> deptList = Company.getDepartmentList();
		for (int i = 0; i < deptList.size(); i++) {
			combo.addItem(deptList.get(i));
		}
		
		TextBox.add(combo);
		
		JButton confirm = new JButton("Confirm");
		confirm.addActionListener(new ActionListener() {
			@SuppressWarnings("static-access")
			public void actionPerformed(ActionEvent e) {
				Department selectedEmp = (Department) combo.getSelectedItem();
				Company.getDepartmentList().remove(selectedEmp);
				SerialSaving.save(Company.getInstance().getDepartmentList(), "departments");
				dispose();
			}
		});
		
		JButton cancel = new JButton("Cancel");
		cancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false); 
				dispose(); 
			}
		});

		Box ButtonBox = Box.createHorizontalBox();
		ButtonBox.add(confirm);
		ButtonBox.add(cancel);
		this.add(TextBox, BorderLayout.CENTER);
		this.add(ButtonBox, BorderLayout.SOUTH);
		this.setVisible(true);
		setLocationRelativeTo(null);
	}
}
