package centralApplication.view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;

import companyModel.Company;
import companyModel.Employee;
import companyModel.SerialSaving;

/**
 * The Class RemoveEmployeeWindow, the GUI to add a department in the company.
 */
public class RemoveEmployeeWindow extends JFrame implements Runnable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * The method run that opens the GUI.
	 */
	@Override
	public void run() {
		this.setTitle("Remove Employee");
		this.setSize(500, 300);
		Box TextBox = Box.createHorizontalBox();

		JComboBox<Employee> combo = new JComboBox<Employee>();
		combo.setMaximumSize(new Dimension(400, 25));

		ArrayList<Employee> employeeList = Company.getEmployeeList();
		for (int i = 0; i < employeeList.size(); i++) {
			combo.addItem(employeeList.get(i));
		}

		TextBox.add(combo);

		JButton confirm = new JButton("Confirm");
		confirm.addActionListener(new ActionListener() {
			@SuppressWarnings("static-access")
			public void actionPerformed(ActionEvent e) {
				Employee selectedEmp = (Employee) combo.getSelectedItem();
				Company.getEmployeeList().remove(selectedEmp);
				SerialSaving.save(Company.getInstance().getEmployeeList(), "employee");
				dispose();
			}
		});

		JButton cancel = new JButton("Cancel");
		cancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				dispose();
			}
		});

		Box ButtonBox = Box.createHorizontalBox();
		ButtonBox.add(confirm);
		ButtonBox.add(cancel);
		this.add(TextBox, BorderLayout.CENTER);
		this.add(ButtonBox, BorderLayout.SOUTH);
		this.setVisible(true);
		setLocationRelativeTo(null);
	}
}
