package centralApplication.view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalTime;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.Timer;
import javax.swing.table.DefaultTableModel;

import centralApplication.controler.CentralAppControler;
import centralApplication.controler.GetCheck;
import companyModel.Company;
import companyModel.SerialSaving;

/**
 * The Class CentralAppWindow, GUI for the central application. This is the main
 * class of the central application. It instantiate the graphics elements for
 * the GUI and the action listeners that go with. This class create the 3 panes
 * of the app (Staff management, Department management and the Global checks
 * history) and keep them updated.
 */
public class CentralAppWindow extends JFrame {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The tab panel. */
	private JTabbedPane tabPane;

	/** The panel for staff management. */
	private JPanel StaffManagement;

	/** The panel for department management. */
	private JPanel DeptManagement;

	/** The panel for checks history. */
	private JPanel CheckInOutHistory;

	/** The scroll panel for staff management. */
	private JScrollPane StaffManagementScroll;

	/** The scroll panel for department management. */
	private JScrollPane DeptManagementScroll;

	/** The scroll panel for checks history. */
	private JScrollPane CheckScroll;

	/** The employee table. */
	private JTable employeeTable;

	/** The department table. */
	private JTable deptTable;

	/** The check table. */
	private JTable checkTable;

	/** The button that open the window to add an employee. */
	private JButton addEmployeeButton;

	/** The button that open the window to remove an employee. */
	private JButton removeEmployeeButton;

	/** The remove that open the window to add a department. */
	private JButton addDeptButton;

	/** The button that open the window to remove a department. */
	private JButton removeDeptButton;

	/** The button box for employee management. */
	private JPanel EmployeeButtonBox;

	/** The button box for department management. */
	private JPanel DeptButtonBox;

	/** The employee table model. */
	public static DefaultTableModel employeeTableModel;

	/** The department table model. */
	public static DefaultTableModel deptTableModel;

	/** The check table model. */
	public static DefaultTableModel checkTableModel;

	/**
	 * Instantiates a new central app window.
	 */
	public CentralAppWindow() {
		this.setTitle("Central application - V1");
		this.setSize(1000, 600);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		tabPane = new JTabbedPane();
		StaffManagement = new JPanel();
		DeptManagement = new JPanel();
		CheckInOutHistory = new JPanel();

		// Employees management
		Object[] employeesColumnNames = { "First Name", "Last Name", "ID", "timeBalance", "Standard Arrival Time",
				"Standard Departure Time" };
		employeeTableModel = new DefaultTableModel(employeesColumnNames, 0);
		for (int i = 0; i < Company.getEmployeeList().size(); i++) {
			String firstname = Company.getEmployeeList().get(i).getFirstname();
			String surname = Company.getEmployeeList().get(i).getSurname();
			int id = Company.getEmployeeList().get(i).getEmployeeId();
			int recoverableHours = Company.getEmployeeList().get(i).getRecoverableHours();
			LocalTime standardArrivalTime = Company.getEmployeeList().get(i).getStdArrivalTime();
			LocalTime standardDepartureTime = Company.getEmployeeList().get(i).getStdLeavingTime();

			Object[] employeeData = { firstname, surname, id, recoverableHours, standardArrivalTime,
					standardDepartureTime };
			employeeTableModel.addRow(employeeData);
		}

		EmployeeButtonBox = new JPanel();
		EmployeeButtonBox.setPreferredSize(new Dimension(600, 200));

		addEmployeeButton = new JButton("Add employee");
		removeEmployeeButton = new JButton("Remove employee");

		EmployeeButtonBox.add(addEmployeeButton);
		EmployeeButtonBox.add(removeEmployeeButton);

		addEmployeeButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				new Thread(new AddEmployeeWindow()).start();
			}
		});

		removeEmployeeButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				new Thread(new RemoveEmployeeWindow()).start();
			}
		});

		employeeTable = new JTable(employeeTableModel);
		StaffManagementScroll = new JScrollPane(employeeTable);
		StaffManagement = new JPanel();
		StaffManagementScroll.setPreferredSize(new Dimension(950, 470));
		StaffManagement.add(StaffManagementScroll, BorderLayout.NORTH);
		StaffManagement.add(EmployeeButtonBox, BorderLayout.SOUTH);

		// Departments management
		Object[] deptColumnNames = { "Department Name", "ID" };
		deptTableModel = new DefaultTableModel(deptColumnNames, 0);
		for (int i = 0; i < Company.getDepartmentList().size(); i++) {
			String name = Company.getDepartmentList().get(i).getDepartmentName();
			int ID = Company.getDepartmentList().get(i).getDepartmentId();

			Object[] deptData = { name, ID };
			deptTableModel.addRow(deptData);
		}

		DeptButtonBox = new JPanel();
		DeptButtonBox.setPreferredSize(new Dimension(600, 200));

		addDeptButton = new JButton("Add department");
		removeDeptButton = new JButton("Remove department");

		DeptButtonBox.add(addDeptButton);
		DeptButtonBox.add(removeDeptButton);

		addDeptButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				new Thread(new AddDepartmentWindow()).start();
			}
		});

		removeDeptButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				new Thread(new RemoveDepartmentWindow()).start();
			}
		});

		deptTable = new JTable(deptTableModel);
		DeptManagementScroll = new JScrollPane(deptTable);
		DeptManagement = new JPanel();
		DeptManagementScroll.setPreferredSize(new Dimension(950, 470));
		DeptManagement.add(DeptManagementScroll, BorderLayout.NORTH);
		DeptManagement.add(DeptButtonBox, BorderLayout.SOUTH);

		// Checks history management
		Object[] checkColumnNames = { "First Name", "Last Name", "ID", "Date", "Time" };
		checkTableModel = new DefaultTableModel(checkColumnNames, 0);

		for (int i = 0; i < Company.getChecksList().size(); i++) {
			String firstname = Company.getChecksList().get(i).getEmployee().getFirstname();
			String surname = Company.getChecksList().get(i).getEmployee().getSurname();
			int id = Company.getChecksList().get(i).getEmployee().getEmployeeId();
			String checkDate = Company.getChecksList().get(i).getDate();
			String checkTime = Company.getChecksList().get(i).getTime();

			Object[] checkData = { firstname, surname, id, checkDate, checkTime };
			checkTableModel.addRow(checkData);
		}

		checkTable = new JTable(checkTableModel);
		CheckScroll = new JScrollPane(checkTable);
		CheckInOutHistory = new JPanel();
		CheckScroll.setPreferredSize(new Dimension(950, 470));
		CheckInOutHistory.add(CheckScroll, BorderLayout.NORTH);

		// Add the 3 tabs to the panel
		tabPane.addTab("Staff Management", null, StaffManagement);
		tabPane.addTab("Department Management", null, DeptManagement);
		tabPane.addTab("Global check in/out history", null, CheckInOutHistory);

		this.getContentPane().add(tabPane);
		this.setVisible(true);

		// Action performed each 500ms, refreshing the view
		ActionListener refreshlistener = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				CentralAppControler.updateSeriasibleContent(employeeTableModel, deptTableModel, checkTableModel);
			}
		};
		new Timer(500, refreshlistener).start();
	}

	/**
	 * The main method. This method launch the central app GUI and the server
	 * linked with it, ready to receive the checks from the time tracker.
	 *
	 * @param args
	 *            the arguments
	 */
	public static void main(String[] args) {

		new CentralAppWindow();
		new Thread(new GetCheck()).start();	
	}

}
