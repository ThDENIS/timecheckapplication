package centralApplication.view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.time.LocalTime;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import companyModel.Company;
import companyModel.Employee;

/**
 * The Class AddEmployeeWindow, the GUI to add a employee in the company.
 */
public class AddEmployeeWindow extends JFrame implements Runnable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * The method run that opens the GUI.
	 */
	public void run() {
		this.setTitle("Add Employee");
		this.setSize(500, 300);
		Box TextBox = Box.createHorizontalBox();

		JLabel label1 = new JLabel("        Enter your employee   ");

		JTextField firstname = new JTextField("Firstname");
		firstname.setMaximumSize(new Dimension(100, 30));
		firstname.addFocusListener(new FocusListener() {
			public void focusGained(FocusEvent e) {
				firstname.setText("");
			}

			public void focusLost(FocusEvent e) {
				if (firstname.getText().equals(""))
					firstname.setText("Firstname");
			}
		});

		TextField lastname = new TextField("Lastname");
		lastname.setMaximumSize(new Dimension(100, 30));
		lastname.addFocusListener(new FocusListener() {
			public void focusGained(FocusEvent e) {
				lastname.setText("");
			}

			public void focusLost(FocusEvent e) {
				if (lastname.getText().equals(""))
					lastname.setText("surname");
			}
		});

		TextField id = new TextField("id");
		id.setMaximumSize(new Dimension(100, 30));
		id.addFocusListener(new FocusListener() {
			public void focusGained(FocusEvent e) {
				id.setText("");
			}

			public void focusLost(FocusEvent e) {
				if (id.getText().equals(""))
					id.setText("id");
			}
		});

		TextBox.add(label1);
		TextBox.add(firstname);
		TextBox.add(lastname);
		TextBox.add(id);

		JButton confirm = new JButton("Confirm");
		confirm.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String Firstname = firstname.getText();
				String Lastname = lastname.getText();
				String strID = id.getText();
				int ID = Integer.parseInt(strID);
				Company.getEmployeeList()
						.add(new Employee(Firstname, Lastname, ID, LocalTime.of(8, 0), LocalTime.of(16, 0)));
				dispose();
			}
		});

		JButton cancel = new JButton("Cancel");
		cancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				dispose();
			}
		});

		add(TextBox, BorderLayout.CENTER);
		Box ButtonBox = Box.createHorizontalBox();
		ButtonBox.add(confirm);
		ButtonBox.add(cancel);
		add(ButtonBox, BorderLayout.SOUTH);
		this.setVisible(true);
		setLocationRelativeTo(null);
	}
}
