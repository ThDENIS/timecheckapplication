package centralApplication.view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import companyModel.Company;
import companyModel.Department;

/**
 * The Class AddDepartmentWindow, the GUI to add a department in the company.
 */
public class AddDepartmentWindow extends JFrame implements Runnable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * The method run that opens the GUI.
	 */
	public void run() {
		this.setTitle("Add Employee");
		this.setSize(500, 300);
		Box TextBox = Box.createHorizontalBox();

		JLabel label1 = new JLabel("        Enter your department   ");
		JTextField name = new JTextField("name");
		name.setMaximumSize(new Dimension(100, 30));
		name.addFocusListener(new FocusListener() {
			public void focusGained(FocusEvent e) {
				name.setText("");
			}

			public void focusLost(FocusEvent e) {
				if (name.getText().equals(""))
					name.setText("name");
			}
		});
		
		TextField ID = new TextField("ID");
		ID.setMaximumSize(new Dimension(100, 30));
		ID.addFocusListener(new FocusListener() {
			public void focusGained(FocusEvent e) {
				ID.setText("");
			}

			public void focusLost(FocusEvent e) {
				if (ID.getText().equals(""))
					ID.setText("ID");
			}
		});
		
		TextBox.add(label1);
		TextBox.add(name);
		TextBox.add(ID);
		
		JButton confirm = new JButton("Confirm");
		confirm.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String Name = name.getText();
				String strID = ID.getText();
				int ID = Integer.parseInt(strID);
				Company.getDepartmentList().add(new Department(Name, ID));
				dispose();
			}
		});
		
		JButton cancel = new JButton("Cancel");
		cancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false); 
				dispose(); 
			}
		});

		add(TextBox, BorderLayout.CENTER);
		Box ButtonBox = Box.createHorizontalBox();
		ButtonBox.add(confirm);
		ButtonBox.add(cancel);
		add(ButtonBox, BorderLayout.SOUTH);
		this.setVisible(true);
		setLocationRelativeTo(null);
	}
}
