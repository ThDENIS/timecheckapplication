package centralApplication.controler;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;

import companyModel.CheckInOut;
import companyModel.Company;

/**
 * The Class GetCheck that received checks from the time tracker app. The class
 * is the server part of the TCP communication. It is open and can receive
 * checks as long as the central app is open.
 */
public class GetCheck implements Runnable {

	/** The TCP socket. */
	protected Socket s;

	/** The TCP server socket. */
	protected ServerSocket ss;

	/** The IP Socket Address. */
	protected InetSocketAddress isA;

	/** The Object Input Stream. */
	protected ObjectInputStream ois;

	/** The check received. */
	protected CheckInOut check;

	/**
	 * Instantiates a new gets the check.
	 */
	public GetCheck() {
		s = null;
		ss = null;
		isA = new InetSocketAddress("localhost", 8080);
		ois = null;
	}

	/**
	 * The method run that opens the server. A server for TCP communication is
	 * opened by the central application. The communication port is 8080. The
	 * server can received checks from the time tracker application for an
	 * unlimited time.
	 */
	public void run() {
		try {
			ss = new ServerSocket();
			ss.bind(isA);

			while (true) {
				s = ss.accept();
				ois = new ObjectInputStream(s.getInputStream());
				CheckInOut check = (CheckInOut) ois.readObject();
				(Company.getChecksList()).add(check);
			}

		} catch (ClassNotFoundException | IOException e) {
			e.printStackTrace();
		} finally {
			try {
				ois.close();
				s.close();
				ss.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}