package centralApplication.controler;

import javax.swing.table.DefaultTableModel;

import companyModel.Company;
import companyModel.SerialSaving;

public class CentralAppControler {

	public static void updateSeriasibleContent(DefaultTableModel employeeTableModel, DefaultTableModel deptTableModel, DefaultTableModel checkTableModel) {
		// Update employee lists
		employeeTableModel.setRowCount(0);
		for (int i = 0; i < Company.getEmployeeList().size(); i++) {
			Object[] empData = { Company.getEmployeeList().get(i).getFirstname(),
					Company.getEmployeeList().get(i).getSurname(),
					Company.getEmployeeList().get(i).getEmployeeId(),
					Company.getEmployeeList().get(i).getRecoverableHours(),
					Company.getEmployeeList().get(i).getStdArrivalTime(),
					Company.getEmployeeList().get(i).getStdLeavingTime(), null, null, null, null, null, null,
					null, null };
			employeeTableModel.addRow(empData);
		}

		// Update department lists
		deptTableModel.setRowCount(0);
		for (int i = 0; i < Company.getDepartmentList().size(); i++) {
			Object[] deptData = { Company.getDepartmentList().get(i).getDepartmentName(),
					Company.getDepartmentList().get(i).getDepartmentId(), null, null };
			deptTableModel.addRow(deptData);
		}

		// Update checks lists
		checkTableModel.setRowCount(0);
		for (int i = 0; i < Company.getChecksList().size(); i++) {
			Object[] checkData = { Company.getChecksList().get(i).getEmployee().getFirstname(),
					Company.getChecksList().get(i).getEmployee().getSurname(),
					Company.getChecksList().get(i).getEmployee().getEmployeeId(),
					Company.getChecksList().get(i).getDate(), 
					Company.getChecksList().get(i).getTime(), null,
					null, null, null, null };
			checkTableModel.addRow(checkData);
		}

		// Save the data
		Company.getInstance();
		SerialSaving.save(Company.getEmployeeList(), "employee");
		SerialSaving.save(Company.getDepartmentList(), "departments");
		SerialSaving.save(Company.getChecksList(), "checks");
	}
}
