package companyModel;

import java.io.Serializable;

/**
 * The abstract Class Person representing a person.
 */
public abstract class Person implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The firstname. */
	String firstname;
	
	/** The surname. */
	String surname;

	/**
	 * Instantiates a new person.
	 */
	public Person() {
		firstname = null;
		surname = null;
	}

	/**
	 * Instantiates a new person.
	 *
	 * @param firstname the firstname
	 * @param surname the surname
	 */
	public Person(String firstname, String surname) {
		this.firstname = firstname;
		this.surname = surname;
	}

	/**
	 * Gets the firstname.
	 *
	 * @return the firstname
	 */
	public String getFirstname() {
		return firstname;
	}

	/**
	 * Sets the firstname.
	 *
	 * @param firstname the new firstname
	 */
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	/**
	 * Gets the surname.
	 *
	 * @return the surname
	 */
	public String getSurname() {
		return surname;
	}

	/**
	 * Sets the surname.
	 *
	 * @param surname the new surname
	 */
	public void setSurname(String surname) {
		this.surname = surname;
	}

	/**
	 * Transform an object Person to a string for using in as such
	 */
	public String toString() {
		return firstname + " " + surname;
	}
}
