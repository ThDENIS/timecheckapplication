package companyModel;

import java.io.Serializable;

/**
 * The abstract Class VirtualDepartment.
 */
public abstract class VirtualDepartment implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The department id. */
	//Attributes
	private int departmentId;
	
	/**
	 * Instantiates a new virtual department.
	 */
	public VirtualDepartment(){
		
	}
	
	/**
	 * Instantiates a new virtual department.
	 *
	 * @param virtualDepartmentParam the virtual department param
	 */
	public VirtualDepartment(VirtualDepartment virtualDepartmentParam) {
		departmentId = virtualDepartmentParam.getDepartmentId();
	}
	
	/**
	 * Instantiates a new virtual department.
	 *
	 * @param departmentId the department id
	 */
	public VirtualDepartment(int departmentId) {
		this.departmentId = departmentId;
	}

	/**
	 * Gets the department id.
	 *
	 * @return the department id
	 */
	//Getters & Setters
	public int getDepartmentId() {
		return departmentId;
	}

	/**
	 * Sets the department id.
	 *
	 * @param departmentId the new department id
	 */
	public void setDepartmentId(int departmentId) {
		this.departmentId = departmentId;
	}

	

}
