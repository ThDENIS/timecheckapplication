package companyModel;

import java.io.Serializable;
import java.time.LocalTime;

/**
 * The Class Employee representing an employee.
 */
public class Employee extends Person implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The employee id. */
	private int employeeId = 0;
	
	/** The std arrival time. */
	private LocalTime stdArrivalTime = LocalTime.of(9, 00);
	
	/** The std leaving time. */
	private LocalTime stdLeavingTime = LocalTime.of(16, 00);
	
	/** The recoverable hours. */
	private int recoverableHours = 0;
	
	/** The department. */
	private Department department = null;

	/**
	 * Instantiates a new employee.
	 */
	public Employee() {
		super();
	}

	/**
	 * Instantiates a new employee.
	 *
	 * @param employeeParam the employee to copy
	 */
	public Employee(Employee employeeParam) {
		super(employeeParam.getFirstname(), employeeParam.getSurname());
		stdArrivalTime = employeeParam.getStdArrivalTime();
		stdLeavingTime = employeeParam.getStdLeavingTime();
		recoverableHours = employeeParam.getRecoverableHours();
		department = employeeParam.getDepartment();
	}

	/**
	 * Instantiates a new employee.
	 *
	 * @param firstName the first name
	 * @param surname the surname
	 * @param ID the id
	 * @param stdArrivalTime the std arrival time
	 * @param stdLeavingTime the std leaving time
	 */
	public Employee(String firstName, String surname, int ID, LocalTime stdArrivalTime, LocalTime stdLeavingTime) {
		super(firstName, surname);
		this.employeeId=ID;
		this.stdArrivalTime = stdArrivalTime;
		this.stdLeavingTime = stdLeavingTime;
		this.recoverableHours = 0;
		this.department = null;
	}

	/**
	 * Gets the employee id.
	 *
	 * @return the employee id
	 */
	public int getEmployeeId() {
		return employeeId;
	}

	/**
	 * Sets the employee id.
	 *
	 * @param employeeId the new employee id
	 */
	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}

	/**
	 * Gets the std arrival time.
	 *
	 * @return the std arrival time
	 */
	public LocalTime getStdArrivalTime() {
		return stdArrivalTime;
	}

	/**
	 * Sets the std arrival time.
	 *
	 * @param stdArrivalTime the new std arrival time
	 */
	public void setStdArrivalTime(LocalTime stdArrivalTime) {
		this.stdArrivalTime = stdArrivalTime;
	}

	/**
	 * Gets the std leaving time.
	 *
	 * @return the std leaving time
	 */
	public LocalTime getStdLeavingTime() {
		return stdLeavingTime;
	}

	/**
	 * Sets the std leaving time.
	 *
	 * @param stdLeavingTime the new std leaving time
	 */
	public void setStdLeavingTime(LocalTime stdLeavingTime) {
		this.stdLeavingTime = stdLeavingTime;
	}

	/**
	 * Gets the recoverable hours.
	 *
	 * @return the recoverable hours
	 */
	public int getRecoverableHours() {
		return recoverableHours;
	}

	/**
	 * Sets the recoverable hours.
	 *
	 * @param recoverableHours the new recoverable hours
	 */
	public void setRecoverableHours(int recoverableHours) {
		this.recoverableHours = recoverableHours;
	}

	/**
	 * Gets the department.
	 *
	 * @return the department
	 */
	public Department getDepartment() {
		return department;
	}

	/**
	 * Sets the department.
	 *
	 * @param department the new department
	 */
	public void setDepartment(Department department) {
		this.department = department;
	}
	
	/**
	 * Transform an object Person to a string for using in as such
	 */
	public String toString(){
		return super.toString() + ", ID = "+employeeId;
	}
}
