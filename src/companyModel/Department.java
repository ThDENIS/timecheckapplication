package companyModel;

/**
 * The Class Department representing a department of the company.
 */
public class Department extends VirtualDepartment {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The department name. */
	private String departmentName;

	/**
	 * Instantiates a new department.
	 */
	public Department() {
		super();
		departmentName = null;
	}

	/**
	 * Instantiates a new department.
	 *
	 * @param departmentParam the department to copy
	 */
	public Department(Department departmentParam) {
		super(departmentParam.getDepartmentId());
		departmentName=departmentParam.getDepartmentName();
	}
	
	/**
	 * Instantiates a new department.
	 *
	 * @param departmentName the department name
	 * @param departmentId the department id
	 */
	public Department(String departmentName, int departmentId) {
		super(departmentId);
		this.departmentName = departmentName;
	}

	/**
	 * Gets the department name.
	 *
	 * @return the department name
	 */
	public String getDepartmentName() {
		return departmentName;
	}

	/**
	 * Sets the department name.
	 *
	 * @param departmentName the new department name
	 */
	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	/**
	 * Transform an object Department to a string for using in as such
	 */
	public String toString() {
		return departmentName+ ", ID = "+getDepartmentId();
	}

}
