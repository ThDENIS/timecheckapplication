package companyModel;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * The Class SerialSaving that allows serialisation of objets. In this class you
 * can save an object in a file by serialazing it. In the other way, you can
 * load an serialized object from a file.
 */
public class SerialSaving {

	/**
	 * Save method. It allows the user to save an object by serialization
	 *
	 * @param object
	 *            the object you want to save
	 * @param string
	 *            the string containing the path of the file where you want to
	 *            save the object
	 */
	public static void save(Object object, String string) {
		ObjectOutputStream oos;
		try {
			oos = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(string)));
			oos.writeObject(object);
			oos.flush();
			oos.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Load method. It allows the user to load an object from serialized object
	 * in a file
	 *
	 * @param string
	 *            the string containing the serialized object
	 * @return the object
	 */
	public static Object load(String string) {
		Object o = null;
		ObjectInputStream ois;
		try {
			ois = new ObjectInputStream(new BufferedInputStream(new FileInputStream(string)));
			o = (Object) ois.readObject();
			ois.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return o;
	}
}
