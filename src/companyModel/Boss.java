package companyModel;

/**
 * The Class Boss.
 */
public class Boss extends Person {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new boss.
	 */
	public Boss() {
		super();
	}

	/**
	 * Instantiates a new boss.
	 *
	 * @param bossParam the boss param
	 */
	public Boss(Boss bossParam) {
		super(bossParam.getFirstname(), bossParam.getSurname());
	}
	
	/**
	 * Instantiates a new boss.
	 *
	 * @param firstname the firstname
	 * @param surname the surname
	 */
	public Boss(String firstname, String surname) {
		super(firstname, surname);
	}

	/**
	 * Gets the company.
	 *
	 * @return the company
	 */
	//Getters & Setters
	public Company getCompany() {
		return null;

	}

	/**
	 * Sets the company.
	 *
	 * @param company the new company
	 */
	public void setCompany(Company company) {

	}

	

}
