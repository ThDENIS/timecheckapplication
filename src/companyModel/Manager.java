/*
 * 
 * 
 * Faire constructeur de recopie et constructeurs avec args
 * 
 * 
 * 
 */

package companyModel;

// TODO: Auto-generated Javadoc
/**
 * The Class Manager.
 */
public class Manager extends Employee {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The management department. */
	//Attributes
	private ManagementDepartment managementDepartment;
	
	/**
	 * Instantiates a new manager.
	 */
	//Constructors
	public Manager() {
		super();
		managementDepartment = null;
	}

	/**
	 * Instantiates a new manager.
	 *
	 * @param managererParam the managerer param
	 */
	public Manager(Manager managererParam) {
		managementDepartment = managererParam.managementDepartment;
	}
	
	/**
	 * Gets the management department.
	 *
	 * @return the management department
	 */
	//Getters & Setters
	public ManagementDepartment getManagementDepartment() {
		return managementDepartment;
	}

	/**
	 * Sets the management department.
	 *
	 * @param managementDepartment the new management department
	 */
	public void setManagementDepartment(ManagementDepartment managementDepartment) {
		this.managementDepartment = managementDepartment;
	}

}
