package companyModel;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;

import timeCheckApplication.controler.DateTimeManagement;

/**
 * The Class CheckInOut representing a check of an employee on the time tracker.
 */
public class CheckInOut implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The date. */
	private String date;

	/** The time. */
	private String time;

	/** The employee. */
	private Employee employee;

	/**
	 * Instantiates a new check in out.
	 */
	public CheckInOut() {
		date = LocalDate.MIN.toString();
		time = LocalTime.MIN.toString();
		employee = null;
	}

	/**
	 * Instantiates a new check in out.
	 *
	 * @param checkInOutParam
	 *            the check to copy
	 */
	public CheckInOut(CheckInOut checkInOutParam) {
		date = checkInOutParam.getDate();
		time = checkInOutParam.getTime();
		employee = checkInOutParam.getEmployee();
	}

	/**
	 * Instantiates a new check in out.
	 *
	 * @param employee
	 *            the employee that checks
	 */
	public CheckInOut(Employee employee) {
		this.date = DateTimeManagement.getDate();
		this.time = DateTimeManagement.getRoundedTime();
		this.employee = employee;
	}

	/**
	 * Gets the date.
	 *
	 * @return the date
	 */
	public String getDate() {
		return date;
	}

	/**
	 * Sets the date.
	 *
	 * @param date
	 *            the new date
	 */
	public void setDate(LocalDate date) {
		this.date = date.toString();
	}

	/**
	 * Sets the date.
	 *
	 * @param date
	 *            the new date
	 */
	public void setDate(String date) {
		this.date = date;
	}

	/**
	 * Gets the time.
	 *
	 * @return the time
	 */
	public String getTime() {
		return time;
	}

	/**
	 * Sets the time.
	 *
	 * @param time
	 *            the new time
	 */
	public void setTime(LocalTime time) {
		this.time = time.toString();
	}

	/**
	 * Sets the time.
	 *
	 * @param time
	 *            the new time
	 */
	public void setTime(String time) {
		this.time = time;
	}

	/**
	 * Gets the employee.
	 *
	 * @return the employee
	 */
	public Employee getEmployee() {
		return employee;
	}

	/**
	 * Sets the employee.
	 *
	 * @param employee
	 *            the new employee
	 */
	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	/**
	 * Transform an object CheckInOut to a string for using in as such
	 */
	public String toString() {
		return employee.firstname + " " + employee.surname + ": " + date + " at " + time;
	}
}
