package companyModel;

import java.util.ArrayList;

/**
 * The Class Company, and singleton that represent the company.
 */
public class Company {
	
	/** The company name. */
	private static String companyName;
	
	/** The department list. */
	private static ArrayList<Department> departmentList;
	
	/** The employee list. */
	private static ArrayList<Employee> employeeList;
	
	/** The checks list. */
	private static ArrayList<CheckInOut> checksList;
	
	/** The boss. */
	private Boss boss;

	/**
	 * Instantiates a new company.
	 */
	@SuppressWarnings("unchecked")
	private Company() {
		companyName = "My Company";
		boss = new Boss("John", "Smith");
		if ((departmentList = (ArrayList<Department>) (SerialSaving.load("departments"))) == null)
			departmentList = new ArrayList<Department>();
		
		if ((employeeList = (ArrayList<Employee>) (SerialSaving.load("employee"))) == null)
			employeeList = new ArrayList<Employee>();
		
		if ((checksList = (ArrayList<CheckInOut>) (SerialSaving.load("checks"))) == null)
			checksList = new ArrayList<CheckInOut>();
	}
	
	/** The instance. */
	private static Company INSTANCE = new Company();
 
	/**
	 * Gets the single instance of Company.
	 *
	 * @return single instance of Company
	 */
	public static Company getInstance()
	{	return INSTANCE;
	}

	/**
	 * Gets the company name.
	 *
	 * @return the company name
	 */
	public static String getCompanyName() {
		return companyName;
	}

	/**
	 * Sets the company name.
	 *
	 * @param companyName the new company name
	 */
	public void setCompanyName(String companyName) {
		Company.companyName = companyName;
	}
	
	/**
	 * Gets the boss.
	 *
	 * @return the boss
	 */
	public Boss getBoss() {
		return boss;
	}

	/**
	 * Sets the boss.
	 *
	 * @param boss the new boss
	 */
	public void setBoss(Boss boss) {
		this.boss = boss;
	}

	/**
	 * Gets the department list.
	 *
	 * @return the department list
	 */
	public static ArrayList<Department> getDepartmentList() {
		return departmentList;
	}

	/**
	 * Sets the department list.
	 *
	 * @param departments the new department list
	 */
	public void setDepartmentList(ArrayList<Department> departments) {
		departmentList = departments;
	}

	/**
	 * Gets the employee list.
	 *
	 * @return the employee list
	 */
	public static ArrayList<Employee> getEmployeeList() {
		return employeeList;
	}

	/**
	 * Sets the employee list.
	 *
	 * @param employees the new employee list
	 */
	public void setEmployeeList(ArrayList<Employee> employees) {
		employeeList = employees;
	}

	/**
	 * Sets the checks list.
	 *
	 * @param cheksList the new checks list
	 */
	public static ArrayList<CheckInOut> getChecksList() {
		return checksList;
	}

	/**
	 * Sets the cheks list.
	 *
	 * @param cheksList the new checks list
	 */
	public static void setChecksList(ArrayList<CheckInOut> checksList) {
		Company.checksList = checksList;
	}


}
