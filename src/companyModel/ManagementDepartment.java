package companyModel;

/**
 * The Class ManagementDepartment.
 */
public class ManagementDepartment extends VirtualDepartment {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The boss. */
	private Boss boss;

	/**
	 * Instantiates a new management department.
	 */
	public ManagementDepartment() {
		super();
		boss = null;
	}

	/**
	 * Instantiates a new management department.
	 *
	 * @param managementDepartmentParam the management department param
	 */
	public ManagementDepartment(ManagementDepartment managementDepartmentParam) {
		super(managementDepartmentParam.getDepartmentId());
		boss = managementDepartmentParam.boss;
	}
	
	/**
	 * Instantiates a new management department.
	 *
	 * @param departmentId the department id
	 * @param boss the boss
	 */
	public ManagementDepartment(int departmentId, Boss boss) {
		super(departmentId);
		this.boss = boss;
	}
	
	/**
	 * Gets the boss.
	 *
	 * @return the boss
	 */
	//Getters & Setters
	public Boss getBoss() {
		return boss;
	}

	/**
	 * Sets the boss.
	 *
	 * @param boss the new boss
	 */
	public void setBoss(Boss boss) {
		this.boss = boss;
	}

}
