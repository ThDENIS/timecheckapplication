package timeCheckApplication.controler;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import companyModel.CheckInOut;
import companyModel.Company;
import companyModel.Employee;
import companyModel.SerialSaving;
import timeCheckApplication.view.TimeTrackerWindow;

public class TimeTrackerControler {

	public static void syncListEmployee(JComboBox<Employee> combo) {
		List<Employee> employeeList = (ArrayList<Employee>) (SerialSaving.load("employee"));
		combo.removeAllItems();
		for (int i = 0; i < employeeList.size(); i++) {
			combo.addItem(employeeList.get(i));
		}
	}

	public static void check(JComboBox<Employee> combo) {

		Employee selectedEmp = (Employee) combo.getSelectedItem();
		try {
			CheckInOut check = new CheckInOut(selectedEmp);
			Thread t = new Thread(new SendCheck(check));
			t.start();

			t.join();
			JFrame frame = new JFrame("");
			JOptionPane.showMessageDialog(frame,
					"Check of " + selectedEmp.getFirstname() + " " + selectedEmp.getSurname() + " send.", "Check send.",
					JOptionPane.INFORMATION_MESSAGE);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
			JFrame frame = new JFrame("");
			JOptionPane.showMessageDialog(frame,
					"Check of " + selectedEmp.getFirstname() + " " + selectedEmp.getSurname()
							+ " was not send. You can't reach the server or the server is down",
					"Check not send.", JOptionPane.ERROR_MESSAGE);
		}

	}

	public static void roundTime(JLabel dateLabel, JLabel realTimeLabel, JLabel roundedTimeLabel) {
		dateLabel.setText(DateTimeManagement.getDate());
		realTimeLabel.setText(DateTimeManagement.getRealTime());
		roundedTimeLabel.setText(" ... let's say " + DateTimeManagement.getRoundedTime());
	}

	public static void serialize() {
		SerialSaving.save(Company.getInstance().getEmployeeList(), "employee");
		SerialSaving.save(Company.getInstance().getDepartmentList(), "departments");
	}
}
