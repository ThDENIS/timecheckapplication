package timeCheckApplication.controler;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.time.temporal.ChronoUnit;
import java.util.Locale;

/**
 * The Class DateTimeManagement, used to get the current date, current time or
 * current time rounded to the nearer quarter.
 */
public class DateTimeManagement {

	/**
	 * Gets the current date.
	 *
	 * @return the date
	 */
	public static String getDate() {
		return LocalDate.now().format(DateTimeFormatter.ofLocalizedDate(FormatStyle.FULL).withLocale(Locale.ENGLISH))
				.toString();
	}

	/**
	 * Gets the current real time.
	 *
	 * @return the real time
	 */
	public static String getRealTime() {
		return LocalTime.now().format(DateTimeFormatter.ofLocalizedTime(FormatStyle.SHORT).withLocale(Locale.ENGLISH))
				.toString();
	}

	/**
	 * Gets the current time rounded to the nearer quarter.
	 *
	 * @return the rounded time
	 */
	public static String getRoundedTime() {
		LocalTime time = LocalTime.now().truncatedTo(ChronoUnit.MINUTES);
		int minutes = time.getMinute();
		minutes = minutes % 15;
		if (minutes <= 7) {
			time = time.minusMinutes((long) minutes);
		} else {
			time = time.plusMinutes((long) (15 - minutes));
		}
		return time.format(DateTimeFormatter.ofLocalizedTime(FormatStyle.SHORT).withLocale(Locale.ENGLISH)).toString();
	}
}
