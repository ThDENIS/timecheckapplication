package timeCheckApplication.controler;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;

import companyModel.CheckInOut;

/**
 * The Class SendCheck that sends checks to the central app. The class is the
 * client part of the TCP communication. It connects to the server each time a
 * check is send.
 */

public class SendCheck extends Thread {

	/** The TCP socket. */
	protected Socket s;

	/** The IP Socket Address. */
	protected InetSocketAddress isA;

	/** The check to send. */
	protected CheckInOut check;

	/**
	 * Instantiates a new send check.
	 *
	 * @param check
	 *            the check
	 */
	public SendCheck(CheckInOut check) {
		this.check = check;
		s = null;
		isA = new InetSocketAddress("localhost", 8080);
	}

	/**
	 * The method run that send a check to the central app. This method allows
	 * TCP communication by connecting the client (the time tracker app) and the
	 * server and send a check.
	 */
	public void run(){
		try {
			s = new Socket();
			s.connect(isA);
			ObjectOutputStream oos = new ObjectOutputStream(s.getOutputStream());
			oos.writeObject(check);
			oos.flush();
			oos.close();
			s.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
