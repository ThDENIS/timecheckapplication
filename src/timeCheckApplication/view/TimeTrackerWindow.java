package timeCheckApplication.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.Timer;

import companyModel.CheckInOut;
import companyModel.Company;
import companyModel.Employee;
import companyModel.SerialSaving;
import timeCheckApplication.controler.DateTimeManagement;
import timeCheckApplication.controler.SendCheck;
import timeCheckApplication.controler.TimeTrackerControler;

/**
 * The Class TimeTrackerWindow, GUI for the time tracker application. This is
 * the main class of the time tracker application. It instantiate the graphics
 * elements for the GUI and the action listeners that go with. This class also
 * send the checks to the central app.
 */
public class TimeTrackerWindow extends JFrame {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The date label. */
	JLabel dateLabel = new JLabel(DateTimeManagement.getDate(), JLabel.CENTER);

	/** The real time label. */
	JLabel realTimeLabel = new JLabel(DateTimeManagement.getRealTime());

	/** The rounded time label. */
	JLabel roundedTimeLabel = new JLabel(" ... let's say " + DateTimeManagement.getRoundedTime());

	/**
	 * The sync button that synchronize the time tracker with the central app.
	 */
	JButton syncButton = new JButton("Sync. infos from main app.");

	/**
	 * The check button that sends a check of an employee to the central app.
	 */
	JButton checkButton = new JButton("Check in/out");

	/** The combobox containing the employee. */
	private static JComboBox<Employee> combo = new JComboBox<Employee>();

	/** The employee list. */
	private static ArrayList<Employee> employeeList = new ArrayList<Employee>();

	/**
	 * Instantiates a new time tracker window.
	 */
	public TimeTrackerWindow() {
		this.setTitle("Time tracker emulator - V1");
		this.setSize(400, 220);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setResizable(false);
		this.getContentPane().setBackground(Color.white);

		Box dateBox = Box.createHorizontalBox();
		dateLabel.setFont(new Font("Arial", 0, 18));
		dateBox.add(dateLabel);
		dateBox.add(Box.createHorizontalStrut(0));

		Box timeBox = Box.createHorizontalBox();
		realTimeLabel.setFont(new Font("Arial", Font.BOLD, 30));
		timeBox.add(realTimeLabel);
		timeBox.add(Box.createHorizontalStrut(10));
		roundedTimeLabel.setFont(new Font("Arial", 0, 15));
		timeBox.add(roundedTimeLabel);

		Box syncButtonBox = Box.createHorizontalBox();
		syncButtonBox.add(Box.createHorizontalStrut(10));
		syncButtonBox.add(syncButton);

		combo.setMaximumSize(new Dimension(190, 25));

		Box checkBox = Box.createHorizontalBox();
		checkBox.add(combo);
		checkBox.add(Box.createHorizontalStrut(10));
		checkBox.add(checkButton);

		Box mainBox = Box.createVerticalBox();
		mainBox.add(dateBox);
		mainBox.add(timeBox);
		mainBox.add(syncButtonBox);
		mainBox.add(checkBox);

		this.getContentPane().add(mainBox);
		this.setVisible(true);

		// Action performed when clicking on Sync button
		syncButton.addActionListener(new ActionListener() {
			@SuppressWarnings("unchecked")
			public void actionPerformed(ActionEvent e) {
				TimeTrackerControler.syncListEmployee(combo);
			}
		});

		// Action performed when clicking on Check button
		checkButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				TimeTrackerControler.check(combo);
			}
		});

		// Action performed each 500ms, keeps the time and date updated
		ActionListener refreshlistener = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				TimeTrackerControler.roundTime(dateLabel, realTimeLabel, roundedTimeLabel);;
			}
		};
		new Timer(500, refreshlistener).start();

		// Action performed on close, saving the data by serialization
		this.addWindowListener(new java.awt.event.WindowAdapter() {
			@Override
			@SuppressWarnings("static-access")
			public void windowClosing(java.awt.event.WindowEvent windowEvent) {
				TimeTrackerControler.serialize();
			}
		});
	}

	/**
	 * The main method. This method loads the list of saved employee then launch
	 * the time tracker GUI.
	 *
	 * @param args
	 *            the arguments
	 */
	@SuppressWarnings({ "unchecked" })
	public static void main(String[] args) {

		employeeList = (ArrayList<Employee>) (SerialSaving.load("employee"));

		new TimeTrackerWindow();
	}

}
